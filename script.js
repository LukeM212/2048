const w = 4, h = 4;

const grid = document.getElementById('grid');
grid.style.width = (w * 100) + 'px';
grid.style.height = (h * 100) + 'px';
const tiles = new Array(w * h).fill(0);
let tileElements = new Array(w * h);
let oldElements = [];

function canCombine(a, b) {
	return a == b;
}

function getIndex(x, y) {
	return y * w + x;
}

function getColour(h) {
	h = (((h % 1) + 1) % 1) * 6;
	let p = Math.floor(h), r = h - p;
	return 'rgb(' + (
		p == 0 ? [1, 1 - r, 0] :
		p == 1 ? [1, 0, r] :
		p == 2 ? [1 - r, 0, 1] :
		p == 3 ? [0, r, 1] :
		p == 4 ? [0, 1, 1 - r] :
		p == 5 ? [r, 1, 0] :
		null).map(v => Math.floor(v * 255)).join(',') + ')';
}

function createElement(index, x, y) {
	let value = tiles[index];
	let tile = document.createElement('div');
	tile.innerHTML = value;
	tile.style.fontSize = Math.min(80, Math.floor(150 / Math.floor(Math.log10(value) + 1))) + 'px';
	tile.style.backgroundColor = getColour(Math.log(value / 2) / 14);
	moveElement(tile, x, y);
	grid.appendChild(tileElements[index] = tile);
}

function moveElement(tile, x, y) {
	tile.style.left = (x * 100) + 'px';
	tile.style.top = (y * 100) + 'px';
}

function removeElement(index) {
	let tile = tileElements[index];
	delete tileElements[index];
	oldElements.push(tile);
}

function slide(getTile) {
	let moved = false;
	for (let i = 0, pos; (pos = getTile(i, 0)); i++) {
		let last, movePos, moveIndex;
		for (let a = 0, b = 0; pos; pos = getTile(i, ++a)) {
			let index = getIndex(...pos), value = tiles[index];
			if (value) {
				let tile = tileElements[index];
				if (canCombine(last, value)) {
					removeElement(index);
					removeElement(moveIndex);
					tiles[moveIndex] += value;
					createElement(moveIndex, ...movePos);
					last = undefined;
				} else {
					movePos = getTile(i, b++);
					moveIndex = getIndex(...movePos);
					tiles[moveIndex] = value;
					delete tileElements[index];
					tileElements[moveIndex] = tile;
					last = value;
				}
				if (index != moveIndex) {
					tiles[index] = 0;
					moveElement(tile, ...movePos);
					moved = true;
				}
			}
		}
	}
	return moved;
}

function addTiles(count) {
	let empty = [];
	for (let y = 0; y < h; y++) for (let x = 0; x < w; x++) if (!tiles[getIndex(x, y)]) empty.push([x, y]);
	for (let i = 0; i < count && tiles.length; i++) {
		let pos = empty.splice(Math.floor(Math.random() * empty.length), 1)[0], index = getIndex(...pos);
		tiles[index] = Math.random() > 0.1 ? 2 : 4;
		createElement(index, ...pos);
	}
}

document.onkeypress = e => {
	let dir = {
		'ArrowUp': (i, j) => { if (i < w && j < h) return [i, j] },
		'ArrowDown': (i, j) => { if (i < w && j < h) return [i, h - 1 - j] },
		'ArrowLeft': (i, j) => { if (j < w && i < h) return [j, i] },
		'ArrowRight': (i, j) => { if (j < w && i < h) return [w - 1 - j, i] },
	}[e.key];
	
	if (dir) {
		for (let e; e = oldElements.pop();) grid.removeChild(e);
		if (slide(dir)) addTiles(1);
	}
};

addTiles(2);

// TODO replace tiles / values with classes
